import RegisterPersonal from "./component/RegisterPersonal";
import RegisterPersonal2 from "./component/RegisterPersonal2";
import RegisterPersonal3 from "./component/RegisterPersonal3";
export const routes = [
  {
    path: "/RegisterPersonal",
    component: RegisterPersonal
  },
  {
    path: "/RegisterPersonal2",
    component: RegisterPersonal2
  },
  {
    path: "/RegisterPersonal3",
    component: RegisterPersonal3
  }
];
