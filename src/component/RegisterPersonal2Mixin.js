export const RegisterPersonal2Mixin = {
  data() {
    return {
      level: null,
      skill: null,
      humanReletion: null,
      communicate: null,
      languageEng: null,
      problemSolve: null,
      knowledge: null,
      prudence: null,
      workArea: null,
      workType: null,
      holiday: null,
      noteHoliday: null,
      idUser: localStorage.idUser,
      idLine: localStorage.idUserLine,
      workArea_1: null,
      workArea_2: null,
      workArea_3: null,
      workArea_4: null,
      workArea_5: null,
      human: []
    };
  },

  methods: {
    hideNote() {
      document.getElementById("noteHoliday").style.display = "block";
    },
    check() {
      var workArea1 = document.getElementById("workArea1");
      var workArea4 = document.getElementById("workArea4");
      var workArea5 = document.getElementById("workArea5");

      if (workArea1.checked == true && this.workArea_1 != null) {
        this.workArea = this.workArea_1;
        this.workArea_2 = null;
        this.workArea_3 = null;
        this.workArea_4 = null;
        this.workArea_5 = null;
      }
      if (workArea4.checked == true && this.workArea_4 != null) {
        this.workArea_4 == "" ? (this.workArea_4 = null) : this.workArea_4;
        this.workArea = this.workArea_4;
        this.workArea_1 = null;
        this.workArea_2 = null;
        this.workArea_3 = null;
        this.workArea_5 = null;
      }

      if (workArea5.checked == true && this.workArea_5 != null) {
        this.workArea_5 == "" ? (this.workArea_5 = null) : this.workArea_5;
        this.workArea = this.workArea_5;
        this.workArea_1 = null;
        this.workArea_2 = null;
        this.workArea_3 = null;
        this.workArea_4 = null;
      } else if (this.workArea_2 != null) {
        this.workArea = this.workArea_2;
        this.workArea_1 = null;
        this.workArea_3 = null;
        this.workArea_4 = null;
        this.workArea_5 = null;
      } else if (this.workArea_3 != null) {
        this.workArea = this.workArea_3;
        this.workArea_1 = null;
        this.workArea_2 = null;
        this.workArea_4 = null;
        this.workArea_5 = null;
      } else {
        // this.workArea = null;
      }
      // this.workArea_1 != null
      //   ? (this.workArea = this.workArea_1 &&
      //     this.workArea_2 = null &&
      //     this.workArea_3 = null && this.workArea_4_ = null &&
      //     this.workArea_5 = null)
      //   : (this.workArea = null);
      // this.workArea_2 != null
      //   ? (this.workArea = this.workArea_2)
      //   : (this.workArea = null);
      // this.workArea_3 != null
      //   ? (this.workArea = this.workArea_3)
      //   : (this.workArea = null);
      // this.workArea_4 != null
      //   ? (this.workArea = this.workArea_4)
      //   : (this.workArea = null);
      // this.workArea_5 != null
      //   ? (this.workArea = this.workArea_5)
      //   : (this.workArea = null);

      console.log("1", this.workArea_1);
      console.log("2", this.workArea_2);
      console.log("3", this.workArea_3);
      console.log("4", this.workArea_4);
      console.log("5", this.workArea_5);
      console.log("area", this.workArea);
      this.workArea_1 = null;
      this.workArea_2 = null;
      this.workArea_3 = null;

      // this.workArea_4 = null;
      // this.workArea_5 = null;
    },
    goback() {
      this.$router.push("/RegisterPersonal");
    },
    nextpage() {
      this.$router.push("/RegisterPersonal3");
    }
  }
};
